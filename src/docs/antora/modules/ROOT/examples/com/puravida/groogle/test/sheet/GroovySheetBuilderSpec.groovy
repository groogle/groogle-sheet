package com.puravida.groogle.test.sheet

import com.google.api.services.drive.DriveScopes
import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.Groogle
import com.puravida.groogle.GroogleBuilder
import com.puravida.groogle.SheetService
import com.puravida.groogle.SheetServiceBuilder
import groovy.sql.Sql
import org.h2.jdbcx.JdbcDataSource
import spock.lang.Shared
import spock.lang.Specification

class GroovySheetBuilderSpec extends Specification{

    @Shared
    Groogle groogle

    void setup(){
        //tag::register[]
        groogle = GroogleBuilder.build {
            withOAuthCredentials {
                applicationName 'test-sheet'
                withScopes SheetsScopes.SPREADSHEETS, DriveScopes.DRIVE
                usingCredentials "src/test/resources/client_secret.json"
                storeCredentials true
            }
            register SheetServiceBuilder.build(), SheetService
        }
        //end::register[]
    }

    def "service registered"(){
        given: "a service"

        SheetService service = groogle.service(SheetService)

        expect:
        service
    }

    def "create sheet"(){
        given: "a service"

        SheetService sheetService = groogle.service(SheetService)

        when: "create a sheet"

        //tag::create[]
        SheetService.WithSpreadSheet withSheet = sheetService.createSpreadSheet("test")
        //end::create[]

        then:
        withSheet.id
    }

    def "create sheet closure"(){
        given: "a service"

        SheetService sheetService = groogle.service(SheetService)

        when: "create a sheet"

        //tag::createClosure[]
        SheetService.WithSpreadSheet withSheet =  sheetService.createSpreadSheet "test", {
            println id
            createSheet 'test'
        }
        //end::createClosure[]

        then:
        withSheet.sheets.size()==2
    }


    def "with spreadSheet"(){
        given: "a service"

        SheetService sheetService = groogle.service(SheetService)

        when: "create a spread"
        SheetService.WithSpreadSheet withSheet = sheetService.createSpreadSheet("test")

        and: "create a sheet"
        withSheet.createSheet("test")

        and: "operate with the spread"
        boolean found = false
        sheetService.withSpreadSheet withSheet.id, {
            found = true
        }

        then:
        withSheet.id

        found
    }

    def "with findOrCreate"(){
        given: "a service"

        SheetService sheetService = groogle.service(SheetService)

        when: "create a spread"
        SheetService.WithSpreadSheet withSheet = sheetService.findOrCreateSpreadSheet("no existe", "test")

        then:
        withSheet
    }


    def "duplicate spreadSheet"(){
        given: "a service"

        SheetService sheetService = groogle.service(SheetService)

        when: "create a spread"
        SheetService.WithSpreadSheet withSheet = sheetService.createSpreadSheet("test", {
            createSheet "test", {
                duplicate "copy from test"
            }
        })

        then:
        withSheet.sheets.size()==3
    }

    def "withCell"(){
        given: "a service"

        SheetService sheetService = groogle.service(SheetService)

        //tag::withCell[]
        when: "update a cell"
        SheetService.WithSpreadSheet sheet = sheetService.createSpreadSheet"test", {
            createSheet "test", {
                cell "A1", "Hola"
                B1 =  "=A1"
                cell "C1", 22
            }
        }

        then:
        sheetService.withSpreadSheet sheet.id, {
            withSheet 'test', {
                assert 'Hola' == cell('A1') as String
                assert 'Hola' == cell('B1') as String

                assert (C1 as int) == 22
                assert (C1 as String) == "22"

                assert 'Hola' == A1
                assert 'Hola' == B1
            }
        }
        //end::withCell[]
    }

    def "write range"(){
        given: "a service"

        SheetService sheetService = groogle.service(SheetService)

        //tag::writeRange[]
        when: "update a cell"
        SheetService.WithSpreadSheet sheet = sheetService.createSpreadSheet"test", {
            createSheet "test", {
                writeRange 'A1', 'C3',{
                    clear()
                    set( [
                            ['Hola','=A1', 22],
                            ['=A1', '', '=C1'],
                    ])
                }
            }
        }

        then:
        sheetService.withSpreadSheet sheet.id, {
            withSheet 'test', {

                assert 'Hola' == cell('A1') as String
                assert 'Hola' == cell('B1') as String

                assert (C1 as int) == 22
                assert (C1 as String) == "22"

                assert B2 == null
            }
        }
        //end::writeRange[]

    }

    def "write query"(){
        given: "a service"

        final SheetService sheetService = groogle.service(SheetService.class);

        and: "a sql"

        //tag::appendSQL[]
        JdbcDataSource ds = new JdbcDataSource()
        ds.url = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1"
        ds.user = ""
        ds.password = ""
        Sql sql = new Sql(ds)
        sql.execute "create table user(id int primary key, name varchar(100), surname varchar(100) )"
        sql.execute"insert into user values(1, 'hello', 'world')"
        sql.execute"insert into user values(2, 'hola', 'mundo')"

        SheetService.WithSpreadSheet withSpreadSheet =
                sheetService.createSpreadSheet "test", {
                        createSheet "sheet", {
                                fromDataSource {
                                    dataSource ds
                                    query "select * from user"
                                    range "B"
                                }
                        }
                }

        //end::appendSQL[]
    }

}
