package com.puravida.groogle

import com.puravida.groogle.impl.GroovySheetService
import groovy.transform.CompileStatic

@CompileStatic
class SheetServiceBuilder {

    static SheetService build(){
        new GroovySheetService()
    }
}
